#answer
#https://github.com/calthoff/tstp/tree/master/part_I/introduction_to_programming/challenges

print("#1")
print("2018/11/14")
print("Python 3.7.1")
print("practice")

###################################
print("")
print("#2")
x = 8

if x <10:
    print("x は10未満です。")
else:   
    print("x は10以上です。")


x = 12

if x <10:
    print("x は10未満です。")
else:   
    print("x は10以上です。")


###################################
print("")
print("#3")

x = 12
print(x)

if x <=10:
    print("x は10以下です。")

elif x < 10 or x <=25:
    print("x は10より大きく25以下の値です。")

else:   
    print("x　は25より大きい値です。")


x = -7
print(x)

if x <=10:
    print("x は10以下です。")

elif x < 10 or x <=25:
    print("x は10より大きく25以下の値です。")

else:   
    print("x　は25より大きい値です。")


x = 100
print(x)

if x <=10:
    print("x は10以下です。")

elif x < 10 or x <=25:
    print("x は10より大きく25以下の値です。")
    

else:   
    print("x　は25より大きい値です。")

    
###################################
print("")
print("#4")

x =10
y= 3

print(x % y)

###################################
print("")
print("#5 商出力 ")


x =10
y= 3

print(x // y)

###################################
print("")
print("#6")


age = 77
retirement = age - 65

if retirement < 10:
    print("You get to retire soon.")
else:
    print("You have a long time until you can retire!")
