#test_hello.py

def print_hello():
    print("Hello")
    print("Hello")

def show_hello():
    print("test_Hello")

def show_number():
    for i in range(0,10):
        print(i)

'''
Traceback (most recent call last):
  File "/Users/nakagawakazusa/Library/Mobile Documents/com~apple~CloudDocs/Python/pyhon/tstp/project.py", line 7, in <module>
    hello.test_hello()
AttributeError: module 'hello' has no attribute 'test_hello'

## reference ##
https://utano.jp/entry/2018/01/python-attribute-error-module-has-no-attribute-new/

[ AttributeError ]

"原因は、 import しようとしているモジュール名と、ファイル名が競合してしまって、
正しくモジュールが import されていないということでした。
解決方法としては helo.py というファイル名を test_hello.py などに変えればOKでした。"

'''
