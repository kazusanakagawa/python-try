#p155


class Rectangle():
    # "__init__" コンストラクタのこと
    #"self"とはインスタンス自身を指し、基本的にはこの引数は必須
    def __init__(self, w, l):
        self.width = w
        self.len = l

    #メソッドは定義されたクラスからしか呼び出すことが出来ない
    def area(self):
        return self.width * self.len

    #メソッドは定義されたクラスからしか呼び出すことが出来ない
    def change_size(self, w, l):
        self.width = w
        self.len = l

#test display
if __name__ == "__main__":
    rectangle = Rectangle(10, 20)
    print(rectangle.area())

    rectangle.change_size(20, 40)
    print(rectangle.area())

