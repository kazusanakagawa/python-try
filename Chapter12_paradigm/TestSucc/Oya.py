#https://www.sejuku.net/blog/28182

class Oya:

    def __init__(self, age, higth):
        print("Oya class : __init__ ")
        self.age = age
        self.higth = higth
        self.hear = 0 # ※1
        self.total = 100 # ※2
        pass
    
    def oya_func(self, days, length):
        self.hear = days * length   
        print("I am OYA")


#test display
if __name__ == "__main__":         
    class Kodomo(Oya):
        def kodomo_func(self, a, b):
            self.total = a + b
            print("I am Kodomo")

#test display
if __name__ == "__main__":

    #class Oya directly call   
    k = Kodomo(20,150)
    print("age : " + str(k.age))
    print("higth : " + str(k.higth))
    print("hear : " + str(k.hear)) #  ※1⬆
    print("total : " + str(k.total)) # ※2⬆︎
    #print("test1\n")
    

    k.oya_func(3,4)
    print(k.hear)
    print("test2\n")

    #call
    k.kodomo_func(0,5)
    print(k.total)
    print("test3\n")

    
