#p52

def f(x):
    return x * 2

f(2)

result = f(2)
print(result)

#p53

def f(y):
    return y + 1

z = f(4)

if z == 5:
    print("z is 5")
else:
    print("z is not 5")

#p53 Modification
def f(y):
    return (y + 1) * 3

z = f(9)

if z == 4:
    print("z is 5")
elif z % 5 == 0:
    print('z is 5 multiple')    
else:
    print("z is not 5")

print(5*3)


#p54
def f():
    return 1 + 1 / 2

result = f()
print(result)


#
def f(x,y,z):
    return x + y + z

result = f(1, 2, 10)
print(result)

if result > 10:
    print('resultは10よりも大きい値です。')
else:
    print('resultは10よりも小さい値です。')


