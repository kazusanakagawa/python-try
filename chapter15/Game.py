#p182 chapter15
from Deck import Deck
from Player import Player

class Game:
    def __init__(self):
        name1 = input("player1 name : ")
        name2 = input("player2 name : ")
        self.deck = Deck()
        self.p1 = Player(name1)
        self.p2 = Player(name2)

    def wins(self, winner):
        w = "this round {} won".format(winner)
        print(w)
        print("")

        # n is name. c is card
    def draw(self, p1n, p1c, p2n, p2c):
        d = "{} is {}、{} is {} drew".format(p1n, p1c, p2n, p2c)
        print(d)
        

    def play_game(self):
        cards = self.deck.cards
        print("Start a war")       
        count = 1
        # len リストの要素を数えてくれる、便利な関数
        while len(cards) >= 2:
            
            print(str(count) + " round")
            m = "End with q, Play with other keys:"
            response = input(m)
            if response == 'q':
                break

            p1c = self.deck.rm_card()
            p2c = self.deck.rm_card()
            p1n = self.p1.name
            p2n = self.p2.name
            self.draw(p1n, p1c, p2n, p2c)
            if p1c > p2c:
                self.p1.wins += 1
                self.wins(self.p1.name)
            else:
                self.p2.wins += 1
                self.wins(self.p2.name)

            count += 1
                
        win = self.winner(self.p1, self.p2)
        if win == "draw":
            print("game over {}!".format(win))
        else:
            print("geme over, << {} >> is winner!".format(win))
    def winner(self, p1, p2):
        if p1.wins > p2.wins:
            return p1.name
        if p1.wins < p2.wins:
            return p2.name
        return "draw"

if __name__ == "__main__":
    game = Game()
    game.play_game()
    
