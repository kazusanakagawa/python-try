#p182 chapter15

class Player:
    def __init__(self, name):
        self.wins = 0
        self.card = None
        self.name = name

if __name__ == "__main__":
    player_name = Player("player1")
    print(player_name.name)

    player_name2 = Player(input("Please enter player's name : "))
    print(player_name2.name)
    
    
