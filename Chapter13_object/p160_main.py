#p160 testDisplay

import p160

display = p160.PublicPrivateExample()
print(display.public)
print(display._unsafe)

display = p160.PublicPrivateExample().public_method()
print(display)


display = p160.PublicPrivateExample()._unsafe_method()
print(display)
