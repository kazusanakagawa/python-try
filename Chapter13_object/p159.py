#p159

class Data:
    def __init__(self):
        self.nums = [1, 2, 3, 4, 5] 

    def change_data(self, index, n):
        self.nums[index] = n

#test display
if __name__ == "__main__":
    data_zero = Data()
    print(data_zero.nums)

    #インスタンスを直接変更
    data_one = Data()
    data_one.nums[0] = 100
    print(data_one.nums)

    #メソッドを通してnumsを変更
    data_two = Data()
    data_two.change_data(0, 100)
    print(data_two.nums)
