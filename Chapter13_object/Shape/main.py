#p165 main class

#from モジュール名 import スーパークラス名
from Shape import Shape

#class サブクラス名 (スーパークラス名):
class Square(Shape):
    pass

a_square = Shape(20, 20)
a_square.print_size()

b_square = Shape(30,20)
b_square.print_size()

c_area = Shape(0, 0)
display = c_area.print_area(30,2)


for roop in range(5) :
    print(str(roop + 1) + " area : " + str(display))
    display *= 10
