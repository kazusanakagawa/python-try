#p166

#from モジュール名 import スーパークラス名
from Shape import Shape

class Override(Shape):
    def area(self):
        return self.width * self.len

    # Override : class Shape //print_size(self) 
    def print_size(self):
        print("I am {} by {}".format(self.width,self.len))

# Override result
a_square = Override(20, 40)
a_square.print_size()

# extra code
# "None" display
print(a_square.print_size())

