#p169 challenge3

from Rectangle1 import Rectangle

#Shape = 形状
class Shape(Rectangle):
    
    def what_am_i(self):
        print("I am a shape {} plus {} by 2 equal {} ".format(self.width,self.length, (self.width + self.length) * 2))

#override
result = Shape(20, 30)
result.what_am_i()


result = Rectangle(20, 10)
result.what_am_i()


#call Rectangle class
result = Rectangle(20, 20)
#print() を入れると "None"を返す
print(result.what_am_i())

print("")


#sample answer

class Shape():
    def what_am_i(self):
        print("I am a shape.")


class Rectangle(Shape):
    def __init__(self, width, length):
        self.width = width
        self.length = length

    def calculate_perimeter(self):
        return self.width * 2 + self.length * 2


class Square(Shape):
    def __init__(self, s1):
        self.s1 = s1

    def calculate_perimeter(self):
        return self.s1 * 4 

a_rectangle = Rectangle(20, 50)
a_square = Square(29)

a_rectangle.what_am_i()
a_square.what_am_i()
