#p169 challenge4

class Horse:
    def __init__(self, name, master):
        self.name = name
        self.master = master

class Rider:
    def __init__(self, name):
        self.name = name

master_name = Rider("master name")
freedom = Horse("FreedomHorse", master_name)
print(freedom.master.name)
print(freedom.name)

print("")

#sample answer
class Horse():
    def __init__(self, name):
        self.name = name


class Rider():
    def __init__(self, name, horse):
        self.name = name
        self.horse = horse

harry_the_horse = Horse("Harry")
the_rider = Rider("Sally", harry_the_horse)

print(the_rider.horse.name)
print(the_rider.name)
