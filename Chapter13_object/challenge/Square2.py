#p169 challenge2

from Rectangle1 import Rectangle

class Square(Rectangle):
    pass

    #上記 "pass"入力していれば、下記の2行はいらない
    #def change_perimeter(self):
    #   return self.width + self.length

result = Rectangle(10, 20)
print(result.change_perimeter())

result2 = Rectangle(-20, 20)
print(result2.change_perimeter())

print("")
#################################################

#sample answer
class Square():
    def __init__(self, s1):
        self.s1 = s1

    #周囲を計算する
    def calculate_perimeter(self):
        return self.s1 * 4

    def change_size(self, new_size):
        self.s1 += new_size

a_square = Square(100)
print(a_square.s1)
print("外周：" + str(a_square.calculate_perimeter()))

a_square.change_size(200)
print(a_square.s1)
print("外周：" + str(a_square.calculate_perimeter()))
