#p169_challenge1

class Rectangle:
    def __init__(self, w, l):
        self.width = w
        self.length = l

    #Perimeter length
    def calculate_perimeter(self):
        return (self.width + self.length) * 2

    def change_perimeter(self):
        return self.width,self.length

    #  by challenge3
    def what_am_i(self):
        print("I am a shape {}".format((self.width + self.length) * 2))

if __name__ == "__main__":
    result = Rectangle(20,30)
    print(result.calculate_perimeter())
