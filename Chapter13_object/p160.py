#p160

class PublicPrivateExample:
    def __init__(self):
        self.public = "safe"
        self._unsafe = "unsafe"

    def public_method(self):
        print("\ntest1 : public_method")
        # client が使用可
        pass # pass文は、文が必須な構文で何もしない場合に使用

 
    def _unsafe_method(self):
        print("\ntest2 : _unsafe_methods")
        # client が使用不可
        pass # pass文は、文が必須な構文で何もしない場合に使用

#test display
if __name__ == "__main__":
    display = PublicPrivateExample()
    print(display.public)
    print(display._unsafe)

