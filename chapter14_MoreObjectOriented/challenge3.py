#p177 challenge3

x = 10
y = 20

if x is 10:
    if y is 20:
        print("x:10, y:20")
else:
    print("Noneじゃない")

x = None
y = 1
if x is y is None:
          print("x はNone　yもNone")
elif x is None:
    if y is 1:
        print("x : None  y : 1")
else:
    print(" x, y は　Noneじゃない：(")


result = 2 is 3
print("〈１〉"  + str(result))

result = "a" is "a"
print("〈２〉" + str(result))


#sample answer
def compare(obj1, obj2):
    return obj1 is obj2

def compare_next(obj1, obj2):
    return obj1 is obj1

print(compare("a", "b"))
print(compare_next("a", "b"))
