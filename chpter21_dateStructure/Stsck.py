#https://tinurl.com/zk24ps6


class Stack:
    def __init__(self):
        self.items = []

    def is_empty(self):
        return self.items == []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        last = len(self.items)-1
        return self.items[last]

    def size(self):
        return len(self.items)

stack = Stack()
print(stack)
print(stack.is_empty())
print(stack.push(1))
print(stack.is_empty())
print(stack.items)

for i in range(0, 9):
    stack.push(i)

print(stack.items)
print()

stack2 = Stack()
stack2.push("test")
print(stack2.items)

print(stack.items + stack2.items)