#p80
fruits = {"Apple":"Red",
          "Banana":"Yellow"}

print(fruits)
print(fruits["Apple"])

#under code is // KeyError: 'Red'になる
#print(fruits["Red"])

print()

#../grc281h

#facts：事実
facts = dict()

#value add
facts["code"] = "fun"
# key reference
print(facts["code"])

facts["Bill"] = "Gates"
facts["founded"] = 1776
facts["bool"] = True

print(facts["Bill"])
print(facts["founded"])
print(facts["bool"])

'''
TypeError: unsupported operand type(s) for +: 'int' and 'str'
#print(facts["founded"] + str(facts["Bill"]))
'''


