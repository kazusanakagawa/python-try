#p119

import math

reader = math.pow(2,3)
print(reader)

#p120

import random

reader = random.randint(0,100)

print(reader)

import statistics

# mean//平均値
nums = [1,3,54,23,56,82,29]
reader = statistics.mean(nums)
print(reader)

# median//中央値
reader = statistics.median(nums)
print(reader)

try:
# mode//最頻値
    reader = statistics.mode(nums)
    print(reader)

# not mode//最頻値がなければ
except statistics.StatisticsError as e:
    print(str(e) + " : 最頻値はありません。")

print("")

# p121 ###############################################

print("p121")

import keyword

print(" ###### Python is keyword True or False ######")

words = ["for","football","is","list","  ", "python","print","1"]

try:
    for show in words:
        reader = keyword.iskeyword(show)
        print(show + " ⇨ " + str(reader))

except TypeError as e:
    print("TypeError : " + str(e))

print(" ==================== ")

reader = keyword.iskeyword("for")
print("for → " + str(reader))
