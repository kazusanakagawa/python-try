#p128

with open("st.txt", "r") as f:
    print(f.read())
    print("")



with open("sample_txt","r") as file:
    print(file.read())

with open("sample_txt","r", encoding= "utf-8") as file:
    print(file.read())

print("##############################")

my_list = []


with open("sample_txt","r", encoding= "utf-8") as file1:
    my_list.append(file1.read())
with open("st.txt", "r") as file2:    
    my_list.append(file2.read())

print(str(my_list) + '\n')
