#p126

st = open("st.txt", "w")
st.write("Hi from Python")
st.close()

#関数が一緒はst.txt上書きされる。
st = open("st.txt", "w", encoding="utf-8")
st.write("Hi from Python // ")
st.write("日本語表示 // encoding →　”utf-8””")
st.close()


#sample_txt = open("sample_txt", "w", encoding="utf-8")
sample_txt = open("sample_txt", "w")

for writer in range(10):
    writer += 1
    sample_txt.write(str(writer) + ':')
    sample_txt.write("Hi from Python\n ")
    sample_txt.write("日本語表示 // encoding →　”utf-8 ”” // update`\n ")
    sample_txt.write("アイウエオ, あいうえお\n\n")

sample_txt.close()
