#p132_challenge.py

#3

import csv

movies = [
    ["Top Gun", "Risky Business", "Minority Report"],
    ["Titanic", "The Revenant", "Inception"],
    ["Training Day", "Man on Fire", "Flight"]
    ]

#file write
with open("st.csv","w",newline='') as csvfile:
    file_writer = csv.writer(csvfile, delimiter =",")
    for movie_list in movies:
        file_writer.writerow(movie_list)
    
    print("\n## file wrote ##")


 #file open
print("##  file open strat ##\n")
with open("movies.csv", "r") as csvfile:
    r = csv.reader(csvfile, delimiter=",")
    for row in r:
        print(",".join(row))
    print("\n## file loaded ##")
    

''' sample

with open("movies.csv", "w") as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=",")
    for movie_list in movies:
        spamwriter.writerow(movie_list)
'''
