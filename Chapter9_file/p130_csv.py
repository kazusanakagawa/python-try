#p130

import csv



try:
    while True:
        print("【 choice number 】")
        check = input( "  1:file writer\n  2:file reader\n  3:health.csv\n")
        int_check = int(check)
        
        if int_check == 1: 
            #file write
            with open("st.csv","w",newline='') as f:
                w = csv.writer(f, delimiter=",")
                w.writerow(["one","two","three"])
                w.writerow(["four","five","six"])
                w.writerow(["1,2,3,4,5,6,7,8,9,10"])
                print("\n## file wrote ##")
                
            
        elif int_check == 2:
            #file open
            print("##  file open strat ##\n")
            with open("st.csv", "r") as file:
                r = csv.reader(file, delimiter=",")
                for row in r:
                    print(",".join(row))
                print("\n## file loaded ##")

        elif int_check == 3:
            print("##  file open strat ##\n")
            with open("health.csv", "r") as file:
                r = csv.reader(file, delimiter=",")
                for row in r:
                    print(",".join(row))
                    if row == (","):
                        print("\n")
                print("\n## file loaded ##")
        else:
            print("Input mistake")
            continue
        break
    
except ValueError as e:
    print("ValueError: " +str(e))
    
