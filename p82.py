#p82

#構文がおかしいらしく動かない。

songs = {"1":"fun",
         "2":"blue",
         "3":"me",
         "4":"floor",
         "5":"live"
         }

try:
    while(True):
        n = input("1から5の数字を入力してください：")
        if n in songs:
            song = songs[n]
            print(song)
            break
        else:
            print("見つかりません。")
            print("再度入力してください。")
            continue
    
        print("")

"""
Errorは自分で実装できるclass
https://pycarnival.com/try_exception/
"""
except ValueError:
    print("数字で入力してください。")
